# Release Notes for Developion/Cache 0.x

## 0.7.0

### Added

-   [`Extension`](./src/web/twig/Extension.php) Added `htmlAttributes` parameter to the [`imageFunction()`](./src/web/twig/Extension.php#L91).

### Changed

-   Rewritten `Yii` aliases into explicit paths and URIs.

### Fixed

-   [`ImageService`](./src/services/ImagesService.php): Removed `StringHelper::toSnakeCase()` from [`storeImageExternal()`](./src/services/ImagesService.php#L248)
-   [`CacheService`](./src/services/CacheService.php): [`clearRenderedContent()`](./src/services/CacheService.php#L16) now removing all imported images from the asset volume before it starts rendering new ones. Fixes the conflict of update to existing file.

## 0.6.2

### Added

-   [`Extension`](./src/web/twig/Extension.php): Added `getOrSetFunction()`, using `\yii\caching\CacheInterface::getOrSet()`

### Changed

### Fixed

-   [`Extension`](./src/web/twig/Extension.php): Updated `setCacheFunction()` to actually use the `\yii\caching\CacheInterface::set()` instead of `\yii\caching\CacheInterface::add()`

## 0.6.1

### Added

-   Added `README.md` and `CHANGELOG.md`.
-   Added `htmlParseFilter` from `Core`.
-   Added handling for error caused by inablity of PHP function `getimagesize` to read the image file.
-   Added relative url handler for image URLs.

### Changed

-   Default template for `imageFunction` moved from `Core` to `Cache`.
-   Renamed `imageFunction` > `config` > `param` > `resx` property to `size`.
-   Updated [README.md](./README.md) to reflect the `imageFunction` change.
-   `htmlParseFilter` retains the original image extension.
-   `Developion\Cache\services\ImagesService::createDirs()` moved to `Developion\Cache\services\ImagesService::__construct()`

### Fixed

-   Fixed a typo in output for external cache clearing.
-   Fixed a typo of settings model property.

## 0.6.0 - 2021-12-09

### Added

-   Moved Image Service/Template/TwigFunction from `Core` to `Cache`.
