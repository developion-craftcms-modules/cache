# Cache

Provides hooks and utilities for caching and prerendering content in CraftCMS.

<!-- ![Banner](./placeholder.png) -->

<p align="center">
<a href="LICENSE"><img src="https://img.shields.io/badge/license-MIT-brightgreen.svg?style=plastic" alt="Software License"></img></a>
<a href="https://packagist.org/packages/developion/cache"><img src="https://img.shields.io/packagist/dt/developion/cache.svg?style=plastic" alt="Total Downloads"></img></a>
<a href="https://gitlab.com/developion-craftcms-modules/cache/-/releases"><img src="https://img.shields.io/badge/Latest%20Release-v0.51.0-blue?style=plastic" alt="Latest Version"></img></a>
</p>

# What the hell is the purpose of this thing?

Even though CraftCMS provides html caching and image transforms out of the box, that content would still need to be rendered before the first visitors try to open your pages. And sure, you can refresh the pages yourself any time you clear cache or update the content, but having CraftCMS do it for you seems like a much better approach.

-   Flexible syntax for rendering responsive images in your twig templates
-   Local storing of 3rd party images for optimised serving, using same syntax as local images
-   Authenticable endpoint for cache clearing from external sources
-   Twig functions for variable caching (not just template!)

# Installation

Installation is super-easy via [Composer](https://getcomposer.org/):

```bash
$ composer require developion/cache
```

or add it by hand to your `composer.json` file.

# Usage

## Rendering responsive image HTML

Using `image` function in a twig template renders a `picture` tag containing `webp`/`jpg` pairs of images based on the provided configuration.

```twig
{{ image(
  image = asset,
  config = config,
  alt = alt,
  imageMobile = imageMobile
)|raw }}
```

> image

Accepts url string or `craft\elements\Asset` object. If the image has already been stored in CMS, it will not be uploaded again. Based on the file name.

> alt

Optional. Used if file name (split words) is not preffered for alt.

> imageMobile

Working name. Optional. If provided, it occupies the first pair of `source` tags for responsive sets.

> config

Object containing configuration for the rendered asset. Properties:

-   `template`: template path to be rendered.<br>Defaults to `developion-cache/components/image`.
-   `fallback`: fallback template used in case there is a problem with the passed image.
-   `htmlAttributes`: an object containing key-value pairs of html attributes.
-   `params`: an array of objects containing definition of responsive sizes the image is rendered in.<br>If this is empty, there will be no output. Properties:
    -   `append`: string to be added at the end of the file name (before extension).
    -   `scaleAndCrop`: if `size` is object, it will scale the image before cropping.
    -   `media`: media query for each image pair.
    -   `size`: integer or object with integer properties `width` and `height`.<br>If it's object, it's cropped with given dimensions. Otherwise it's scaled.<br>Defaults to 0 (doesn't change image size)

The simplest working example would look like this:

```twig
{{ image(
  image = 'www.example.com/image.jpg',
  config = {
    params: [
      {
        size: 200
      }
    ]
  }
)|raw }}
```

### Note about `config` parameter

Due to way the php function <a href="https://www.php.net/manual/en/function.array-merge.php">`array_merge`</a> works, it's not possible to provide the default array of `params` without having it in all responsive sets. We are looking for a way to make this possible, but for now, <u>if you are not getting any output, make sure you have an array of objects in your `params` property of `config`.</u>

### Note on `raw` filter

By default, twig escapes all html that comes from variables and functions. To avoid that, use `|raw` filter.

## External source cache clearing

The access to cache clearing from external source can be found on this endpoint:

```url
POST <SITE_URL>/cache/clear
```

The credentials for that endpoint is configured in the project `.env` file like so:

```shell
CLEAR_CACHE_USER="basic_auth_username"
CLEAR_CACHE_PASS="basic_auth_password"
```
