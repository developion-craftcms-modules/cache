<?php

namespace Developion\Cache\events;

use Craft;
use Yii;
use yii\base\Event;

class CachedImageEvent extends Event
{
	/**
	 * @var string $imageUrl Set the fallback image if the requested image couldn't be rendered.
	 */
	public $imageUrl = '';

	public function __construct(array $config = [])
	{
		/** @var \craft\web\AssetManager $assetManager */
		$assetManager = Craft::$app->getView()->getAssetManager();
		$url = $assetManager->getPublishedUrl(
				'@Developion/Cache/web/assets/front/dist',
				true,
				'thumbnail-fallback-square.png',
			);
		$config['imageUrl'] = explode('?', $url)[0];
		parent::__construct($config);
	}
}
