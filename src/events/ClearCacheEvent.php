<?php

namespace Developion\Cache\events;

use yii\base\Event;

class ClearCacheEvent extends Event
{
	const EVENT_BEFORE_CLEAR_CACHE = 'beforeClearCache';

	/**
	 * @var callable[]
	 */
	public $callbacks = [];
}
