<?php

namespace Developion\Cache\jobs;

use Craft;
use craft\helpers\UrlHelper;
use craft\queue\BaseJob;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use yii\queue\RetryableJobInterface;

class RenderContent extends BaseJob implements RetryableJobInterface
{
	protected array $pages = [];

	/** @inheritDoc */
	public function __construct(array $pages, $config = [])
	{
		$this->pages = $pages;
		$this->description = \Craft::t('app', 'Regenerating cached images.');
		parent::__construct($config);
	}
	/**
	 * @inheritdoc
	 */
	public function execute($queue): void
	{
		$total = count($this->pages);
		$client = new Client();
		try {
			foreach ($this->pages as $i => $page) {
				$this->setProgress(
					$queue,
					$i / $total,
					Craft::t(
						'developion-artist',
						'{step, number} of {total, number}',
						[
							'step' => $i + 1,
							'total' => $total,
						]
					)
				);
				$client->get(UrlHelper::siteUrl($page), [
					RequestOptions::DELAY => 1000,
				]);
			}
		} catch (\Throwable $e) {
			Craft::debug($e->getMessage(), 'render-error');
			\Craft::warning("Something went wrong: {$e->getMessage()}", __METHOD__);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function getTtr()
	{
		return 3600;
	}

	/**
	 * @inheritdoc
	 */
	public function canRetry($attempt, $error)
	{
		return $attempt < 3;
	}

	/**
	 * @inheritdoc
	 */
	protected function defaultDescription(): string
	{
		return \Craft::t('app', 'Regenerate cached images.');
	}
}
