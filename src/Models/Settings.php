<?php

namespace Developion\Cache\Models;

use Craft;
use craft\base\Model;
use Developion\Cache\Plugin;

class Settings extends Model
{
	public string $renderPath = 'api-render';

	public string $tmpPath = 'tmp';

	public string $volumePath = 'imported';

	public function attributeLabels()
	{
		return [
			'renderPath' => Craft::t(Plugin::getInstance()->handle, 'Content render path'),
			'tmpPath' => Craft::t(Plugin::getInstance()->handle, 'Temporary upload path'),
			'volumePath' => Craft::t(Plugin::getInstance()->handle, 'Volume Path'),
		];
	}

	public function attributeHints()
	{
		return [
			'renderPath' => Craft::t(Plugin::getInstance()->handle, 'Default content render path.'),
			'tmpPath' => Craft::t(Plugin::getInstance()->handle, 'Default temporary upload path.'),
			'volumePath' => Craft::t(Plugin::getInstance()->handle, 'Default volume upload path.'),
		];
	}
}
