<?php

namespace Developion\Cache;

use Craft;
use craft\base\Plugin as BasePlugin;
use craft\events\RegisterCacheOptionsEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\utilities\ClearCaches;
use craft\web\UrlManager;
use craft\web\View;
use Developion\Cache\Models\Settings;
use Developion\Cache\services\CacheService;
use Developion\Cache\services\ImagesService;
use Developion\Cache\web\twig\Extension;
use Developion\Core\events\DevelopionPluginEvent;
use Developion\Core\traits\IsDevelopionPlugin;
use yii\base\Event;

/**
 * Class Plugin
 *
 * @package developion/cache
 *
 * @property Plugin $plugin
 * @property CacheService $caches
 * @property ImagesService $images
 */
class Plugin extends BasePlugin
{
	use IsDevelopionPlugin;

	public $schemaVersion = '0.7.0';

	protected function _events(): void
	{
		Event::on(
			DevelopionPluginEvent::class,
			DevelopionPluginEvent::EVENT_AT_PLUGIN_INIT,
			function(DevelopionPluginEvent $event) {
				$callbacks = $event->callbacks;
				$callbacks[] = function() {
					$this->setComponents([
						'caches' => CacheService::class,
						'images' => ImagesService::class,
					]);
					Craft::$app->view->registerTwigExtension(new Extension);
				};
				$event->callbacks = $callbacks;
				return $event;
			}
		);

		Event::on(
			UrlManager::class,
			UrlManager::EVENT_REGISTER_SITE_URL_RULES,
			function (RegisterUrlRulesEvent $event) {
				$event->rules['POST cache/clear'] = "{$this->id}/cache/clear-cache";
			}
		);

		Event::on(
			ClearCaches::class,
			ClearCaches::EVENT_REGISTER_CACHE_OPTIONS,
			function (RegisterCacheOptionsEvent $event) {
				$options = $event->options;
				$options[] = [
					'key' => 'api-storage',
					'label' => 'API Images',
					'info' => 'Clears any image files generated from the API.',
					'action' => [$this::$plugin->caches, 'clearRenderedContent'],
				];
				$event->options = $options;
				return $event;
			}
		);

		Event::on(
			View::class,
			View::EVENT_REGISTER_SITE_TEMPLATE_ROOTS,
			function (RegisterTemplateRootsEvent $event) {
				$event->roots['developion-cache'] = __DIR__ . '/templates';
			}
		);
	}

	protected function createSettingsModel()
	{
		return new Settings();
	}
}
