<?php

namespace Developion\Cache\services;

use Craft;
use craft\base\Component;
use craft\base\VolumeInterface;
use craft\elements\Asset;
use craft\errors\ImageException;
use craft\helpers\Session;
use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;
use craft\image\Raster;
use craft\models\VolumeFolder;
use craft\services\AssetIndexer;
use craft\services\Path;
use craft\volumes\Local;
use Developion\Cache\events\CachedImageEvent;
use Developion\Cache\models\Settings;
use Developion\Cache\Plugin;
use Developion\Core\Core;
use Yii;
use yii\base\Event;

class ImagesService extends Component
{
	const EVENT_IF_IMAGE_ERROR = 'imageErrorEvent';

	const EVENT_IF_IMAGE_NOT_STORED = 'imageNotStoredEvent';

	/** @var VolumeFolder|null $folder */
	public $folder;

	public string $renderPath = '';

	public string $importedPath = '';

	public string $tmpPath = '';

	public function __construct($config = [])
	{
		/** @var Path */
		$pathService = Craft::$app->getPath();
		/** @var Settings */
		$cacheSettings = Core::getInstance()->db->getPluginSettings(Plugin::getInstance());
		$this->renderPath	= $cacheSettings['renderPath'];
		$this->importedPath	= $cacheSettings['volumePath'];
		$this->tmpPath		= $cacheSettings['tmpPath'];

		$tmpDir = sprintf(
			'%s/%s',
			$pathService->getStoragePath(),
			$this->tmpPath
		);

		if (!is_dir($tmpDir)) {
			mkdir($tmpDir);
			chmod($tmpDir, 0777);
		}

		if (!is_dir(Yii::getAlias(sprintf(
			'@webroot/%s',
			$this->renderPath
		)))) {
			mkdir(Yii::getAlias(sprintf(
				'@webroot/%s',
				$this->renderPath
			)));
			chmod(Yii::getAlias(sprintf(
				'@webroot/%s',
				$this->renderPath
			)), 0777);
		}

		if (!$this->folder) {
			$this->folder = Craft::$app->getAssets()
				->findFolder([
					'name' => $this->importedPath
				]);
		}
		if (!is_dir(Yii::getAlias(sprintf(
			'%s/%s',
			$this->getVolume()->path,
			$this->importedPath
		)))) {
			mkdir(Yii::getAlias("{$this->getVolume()->path}/{$this->importedPath}"));
			chmod(Yii::getAlias("{$this->getVolume()->path}/{$this->importedPath}"), 0777);
			/** @var AssetIndexer $assetIndexerService */
			$assetIndexerService = Craft::$app->getAssetIndexer();
			$sessionId = $assetIndexerService->getIndexingSessionId();
			$volumeIds = Craft::$app->getVolumes()->getViewableVolumeIds();

			$missingFolders = [];
			$skippedFiles = [];

			foreach ($volumeIds as $volumeId) {
				$indexList = $assetIndexerService->prepareIndexList($sessionId, $volumeId);

				if (!empty($indexList['error'])) {
					return;
				}

				if (isset($indexList['missingFolders'])) {
					$missingFolders += $indexList['missingFolders'];
				}

				if (isset($indexList['skippedFiles'])) {
					$skippedFiles = $indexList['skippedFiles'];
				}

				$response['volumes'][] = [
					'volumeId' => $volumeId,
					'total' => $indexList['total'],
				];
			}

			if (!Craft::$app->request->getIsConsoleRequest()) {
				Session::set('assetsVolumesBeingIndexed', $volumeIds);
				Session::set('assetsMissingFolders', $missingFolders);
				Session::set('assetsSkippedFiles', $skippedFiles);
			}
		}

		parent::__construct($config);
	}

	/**
	 * Renders URL of the passed CMS Asset or Image URL. Returns original URL if it’s not valid.
	 *
	 * @param Asset|string $asset
	 * @param array $config
	 * @param boolean $returnAsset Whether to return the asset object
	 * 
	 * @return Asset|string|bool
	 */
	public function storeImage($asset, array $config = [], $returnAsset = false)
	{
		if ('string' == gettype($asset)) {
			if (stripos($asset, 'http') === false) $asset = UrlHelper::siteUrl($asset);
			if (!$assetUpload = $this->storeImageExternal($asset)) {
				$event = new CachedImageEvent();
				Event::trigger(
					self::class,
					self::EVENT_IF_IMAGE_NOT_STORED,
					$event
				);
				$assetUpload = $this->storeImageExternal($event->imageUrl);
			}
			if (!$assetUpload) {
				throw new ImageException(Craft::t(Plugin::getInstance()->id, sprintf("Image %s can't be stored.", $asset)));
			}
			$asset = $assetUpload;
		}
		$config = array_merge([
			'size' => 0,
			'extension' => 'webp',
			'append' => '',
			'scaleAndCrop' => false
		], $config);
		/** @var int|array $size */
		$size = 0;
		/** @var string $extension */
		$extension = 'webp';
		/** @var string $append */
		$append = '';
		/** @var bool $scaleAndCrop */
		$scaleAndCrop = false;
		extract($config);
		if (!in_array($asset->getMimeType(), [
			'image/png',
			'image/jpeg',
			'image/webp'
		])) {
			return $returnAsset ? $asset : $asset->getUrl();
		}
		/** @var Asset $asset */
		$volumePath = $asset->getVolume()->settings['path'];
		$folderPath = $asset->getFolder()->path;
		$rawName = substr($asset->filename, 0, strrpos($asset->filename, '.'));
		$output_file = sprintf(
			'%s/web/%s/%s%s.%s',
			CRAFT_BASE_PATH,
			$this->renderPath,
			$rawName,
			$append,
			$extension,
		);

		if (file_exists($output_file)) {
			list($width, $height) = getimagesize($output_file);
			if ($size === 0 || $size === $width || (is_array($size) && $size['width'] === $width)) {
				return $returnAsset ? $asset : UrlHelper::siteUrl(sprintf(
					'%s/%s%s.%s',
					$this->renderPath,
					$rawName,
					$append,
					$extension,
				));
			}
		}

		try {
			$image = new Raster();
			$image->loadImage(Yii::getAlias("$volumePath/$folderPath{$asset->filename}"));

			if (is_array($size)) {
				if ($scaleAndCrop) {
					$image->scaleAndCrop($asset->width * $size['height'] / $size['width'], $asset->height, true, $asset->getFocalPoint());
				} else {
					$focal = $asset->getFocalPoint();
					$full_width = $full_height = false;
					if ($size['width'] >= $asset->width) $full_width = true;
					if ($size['height'] >= $asset->height) $full_height = true;
					$x1 = $x2 = $y1 = $y2 = 0;
					if ($full_width) {
						$x1 = 0;
						$x2 = $asset->width;
					} else {
						$x1 = $focal['x'] * $asset->width - $size['width'] / 2;
						$x2 = $focal['x'] * $asset->width + $size['width'] / 2;
						if ($x1 < 0) {
							$xoffset = 0 - $x1;
							$x1 = 0;
							$x2 = $x2 + $xoffset;
						}
						if ($x2 > $asset->width) {
							$xoffset = $x2 - $asset->width;
							$x2 = $asset->width;
							$x1 = $x1 - $xoffset;
						}
					}
					if ($full_height) {
						$y1 = 0;
						$y2 = $asset->height;
					} else {
						$y1 = $focal['y'] * $asset->height - $size['height'] / 2;
						$y2 = $focal['y'] * $asset->height + $size['height'] / 2;
						if ($y1 < 0) {
							$yoffset = 0 - $y1;
							$y1 = 0;
							$y2 = $y2 + $yoffset;
						}
						if ($y2 > $asset->height) {
							$yoffset = $y2 - $asset->height;
							$y2 = $asset->height;
							$y1 = $y1 - $yoffset;
						}
					}
					$image->crop($x1, $x2, $y1, $y2);
				}
			} else {
				if ($size !== 0) {
					$image->scaleToFit($size);
				}
			}

			if ($image->saveAs($output_file)) {
				return $returnAsset ? $asset : UrlHelper::siteUrl(sprintf(
					'%s/%s%s.%s',
					$this->renderPath,
					$rawName,
					$append,
					$extension,
				));
			}
			return $returnAsset ? $asset : $asset->getUrl();
		} catch (\Throwable $th) {
			Yii::debug($th->getTrace(), 'developion-debug');
			$event = new CachedImageEvent();
			Event::trigger(
				self::class,
				self::EVENT_IF_IMAGE_ERROR,
				$event
			);
			return $event->imageUrl;
		}
	}

	/**
	 * Generates CMS Asset from given URL. Returns false if URL isn’t valid.
	 *
	 * @param string $url
	 * @return Asset|bool
	 */
	protected function storeImageExternal(string $url)
	{
		if (!$this->readTest($url)) return false;

		$fileInfo = pathinfo($url);
		$tmpPath = sprintf('%s/%s/%s', Craft::$app->getPath()->getStoragePath(), $this->tmpPath, $fileInfo['basename']);
		file_put_contents($tmpPath, file_get_contents($url));

		$asset = Asset::find()
			->where(['filename' => urldecode($fileInfo['basename'])])
			->one();

		if ($asset) {
			if ($this->readTest(UrlHelper::siteUrl($asset->getUrl()))) return $asset;
			else Craft::$app->elements->deleteElement($asset, true);
		}

		if (!$this->folder) {
			return false;
		}

		$asset = new Asset();
		$asset->tempFilePath = $tmpPath;
		$asset->title = StringHelper::titleizeForHumans(urldecode($fileInfo['basename']));
		$asset->filename = urldecode($fileInfo['basename']);
		$asset->newFolderId = $this->folder->id;
		$asset->volumeId = $this->folder->volumeId;
		$asset->avoidFilenameConflicts = false;
		$asset->setScenario(Asset::SCENARIO_CREATE);

		$result = Craft::$app->getElements()->saveElement($asset, false);

		if ($result) {
			return $asset;
		}
		return false;
	}

	/**
	 * Test for redirects, availability and validity of image URL.
	 *
	 * @param string $url
	 * @return bool
	 */
	public function readTest(string $url): bool
	{
		if (@get_headers($url)[0] == 'HTTP/1.1 302 Found') {
			$url = StringHelper::slice(@get_headers($url)[7], strlen('Location: '));
		}

		if (@get_headers($url)[0] !== 'HTTP/1.1 200 OK') {
			return false;
		}
		try {
			getimagesize($url);
		} catch (\Throwable $th) {
			return false;
		}

		return true;
	}

	public function getVolume(): VolumeInterface
	{
		$assetVolumes = Craft::$app->getVolumes()->getAllVolumes();
		/** @var Local|false */
		$defaultVolume = array_shift($assetVolumes);
		if (!$defaultVolume) {
			$defaultVolume = new Local([
				'name' => 'Blog Images',
				'handle' => 'blogImages',
				'hasUrls' => true,
				'titleTranslationMethod' => 'site',
				'titleTranslationKeyFormat' => null,
				'url' => UrlHelper::siteUrl('blogImages'),
				'path' => sprintf('%s/web/blogImages', CRAFT_BASE_PATH),
			]);
			Craft::$app->getVolumes()->saveVolume($defaultVolume);
		}

		return $defaultVolume;
	}
}
