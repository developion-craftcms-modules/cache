<?php

namespace Developion\Cache\services;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\helpers\FileHelper;
use craft\helpers\Queue;
use Developion\Cache\events\DeleteRenderedContentEvent;
use Developion\Cache\jobs\RenderContent;
use Developion\Cache\Plugin;
use Developion\Core\Core;
use Yii;
use yii\base\Event;

class CacheService extends Component
{
	public function clearRenderedContent(): void
	{
		$cachePlugin = Plugin::getInstance();
		$folder = $cachePlugin->images->folder;
		$assets = Asset::find()
			->volumeId($cachePlugin->images->getVolume()->id)
			->folderId($folder->id)
			->all();
		foreach ($assets as $asset) {
			try {
				Craft::$app->getElements()->deleteElement($asset, true);
			} catch (\Throwable $th) {
				Yii::debug($asset, 'developion/cache/debug');
			}
		}
		$renderPath = Core::getInstance()->db->getPluginSetting($cachePlugin, 'renderPath');
		$path = sprintf(
			'%s/web/%s',
			CRAFT_BASE_PATH,
			$renderPath
		);
		if (!is_dir($path)) FileHelper::createDirectory($path);
		FileHelper::clearDirectory($path);
		$event = new DeleteRenderedContentEvent(['pages' => []]);
		Event::trigger(
			DeleteRenderedContentEvent::class,
			DeleteRenderedContentEvent::EVENT_AFTER_DELETE_RENDERED_CONTENT,
			$event
		);
		Queue::push(new RenderContent($event->pages));
	}
}