<?php

namespace Developion\Cache\web\twig;

use Closure;
use Craft;
use craft\elements\Asset;
use Developion\Cache\events\CachedImageEvent;
use Developion\Cache\Plugin;
use Developion\Cache\services\ImagesService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use yii\base\Event;

class Extension extends AbstractExtension
{
	public function getFunctions(): array
	{
		return [
			// Var Caching
			new TwigFunction('getCache', [$this, 'getCacheFunction']),
			new TwigFunction('setCache', [$this, 'setCacheFunction']),
			new TwigFunction('existsCache', [$this, 'existsCacheFunction']),
			// Image prerendering
			new TwigFunction('image', [$this, 'imageFunction']),
			new TwigFunction('storeImage', [Plugin::getInstance()->images, 'storeImage']),
			new TwigFunction('thumbnailFallback', [$this, 'thumbnailFallbackFunction']),
		];
	}

	public function getFilters(): array
	{
		return [
			new TwigFilter('htmlParse', [$this, 'htmlParseFilter']),
			new TwigFilter('getOrSetCache', [$this, 'getOrSetCacheFilter']),
		];
	}

	public function getCacheFunction(string $key): mixed
	{
		return Craft::$app->cache->get($key);
	}

	public function setCacheFunction(string $key, mixed $value): void
	{
		Craft::$app->cache->set($key, $value);
	}

	public function existsCacheFunction(string $key): bool
	{
		return Craft::$app->cache->exists($key);
	}

	public function getOrSetCacheFilter(string $key, Closure $callback): mixed
	{
		return Craft::$app->cache->getOrSet($key, $callback);
	}

	/**
	 * Renders responsive image template.
	 *
	 * @param Asset|string $image
	 * @param array $config
	 * @param Asset|string|null $imageMobile
	 * @param string $alt
	 * @return string
	 */
	public function imageFunction(Asset|string $image, $config = [], Asset|string $imageMobile = null, string $alt = ''): string
	{
		$thumbs = $mobile = [];
		$config = array_merge([
			'template' => 'developion-cache/components/image',
			'params' => [],
			'htmlAttributes' => []
		], $config);
		/** @var array $params */
		$params = [];
		/** @var string $template */
		$template = 'developion-cache/components/image';
		/** @var array $htmlAttributes */
		$htmlAttributes = [];
		extract($config);
		$sizes = [];
		$imageAsset = Asset::class === gettype($image) ? $image : Plugin::getInstance()->images->storeImage($image, [], true);
		foreach ($params as $paramKey => $param) {
			$thumbs['webp'][$paramKey] = Plugin::getInstance()->images->storeImage($imageAsset, $param);
			if ($paramKey == 0) $sizes = @getimagesize($thumbs['webp'][$paramKey]);
			$thumbs['jpeg'][$paramKey] = Plugin::getInstance()->images->storeImage($imageAsset, array_merge($param, ['extension' => $imageAsset->getExtension() == 'png' ? 'png' : 'jpg']));
		}
		if ($imageMobile && !empty($params)) {
			$imageAssetMobile = Plugin::getInstance()->images->storeImage($imageMobile, [], true);
			$mobile['webp'] = Plugin::getInstance()->images->storeImage($imageAssetMobile, $params[0]);
			$mobile['jpeg'] = Plugin::getInstance()->images->storeImage($imageAssetMobile, array_merge($params[0], ['extension' => $imageAsset->getExtension() == 'png' ? 'png' : 'jpg']));
		}

		$html = Craft::$app->getView()->renderTemplate($template, [
			'sizes' => $sizes,
			'params' => $params,
			'thumbs' => $thumbs,
			'mobile' => $mobile,
			'alt' => $alt ?: $imageAsset->title,
			'htmlAttributes' => $htmlAttributes,
		]);

		return $html;
	}

	public function htmlParseFilter(string $html): string
	{
		if (empty($html)) return '';
		$html = preg_replace_callback('/(src=\\")(.*?)(\\")/', function ($matches) {
			$extension = pathinfo($matches[2])['extension'];
			return $matches[1] . Plugin::getInstance()->images->storeImage($matches[2], [
				'extension' => $extension
			]) . $matches[3];
		}, $html);
		return $html;
	}

	public function thumbnailFallbackFunction(): string
	{
		$event = new CachedImageEvent();
		Event::trigger(
			ImagesService::class,
			ImagesService::EVENT_IF_IMAGE_NOT_STORED,
			$event
		);
		return $event->imageUrl;
	}
}
