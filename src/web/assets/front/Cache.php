<?php

namespace Developion\Cache\web\assets\front;

use craft\web\AssetBundle;

class Cache extends AssetBundle
{
	/** @inheritDoc */
	public function init(): void
	{
		$this->sourcePath = __DIR__ . '/dist';

		$this->depends = [];

		parent::init();
	}
}
