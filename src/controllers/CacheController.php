<?php

namespace Developion\Cache\controllers;

use Craft;
use craft\helpers\FileHelper;
use craft\utilities\ClearCaches;
use craft\web\Controller;
use Developion\Cache\events\ClearCacheEvent;
use Developion\Cache\jobs\RenderContent;
use InvalidArgumentException;
use Throwable;
use yii\base\Event;
use yii\caching\TagDependency;
use yii\web\Response;

class CacheController extends Controller
{
	/**
	 * @inheritdoc
	 */
	protected $allowAnonymous = true;

	/**
	 * @inheritdoc
	 */
	public $enableCsrfValidation = false;

	public function actionClearCache(): Response
	{
		$pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
		$user = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
		if (!getenv('CLEAR_CACHE_PASS') || !getenv('CLEAR_CACHE_USER')) {
			return $this->asJson([
				'status' => 'error',
				'message' => "'CLEAR_CACHE' credentials not configured in project environment.",
			]);
		}
		if ($pass != getenv('CLEAR_CACHE_PASS') || $user != getenv('CLEAR_CACHE_USER')) {
			return $this->asJson([
				'status' => 'error',
				'message' => 'Invalid Credentials.',
			]);
		}
		foreach (Craft::$app->getQueue()->getJobInfo() as $job) {
			$jobDetails = Craft::$app->getQueue()->getJobDetails($job['id']);
			if (get_class($jobDetails['job']) == RenderContent::class) {
				return $this->asJson([
					'status' => 'error',
					'message' => "Cache Clearing already in queue. Currently at {$jobDetails['progress']}%.",
				]);
			}
		}

		$event = new ClearCacheEvent([
			'callbacks' => [],
		]);
		Event::trigger(
			ClearCacheEvent::class,
			ClearCacheEvent::EVENT_BEFORE_CLEAR_CACHE,
			$event
		);
		
		foreach ($event->callbacks as $callback) {
			if (is_callable($callback)) $callback();
		}

		foreach (ClearCaches::cacheOptions() as $cacheOption) {
			$action = $cacheOption['action'];

			if (is_string($action)) {
				try {
					/** @throws InvalidArgumentException|Throwable */
					FileHelper::clearDirectory($action);
				} catch (InvalidArgumentException $e) {
					Craft::warning("Invalid directory {$action}: " . $e->getMessage(), __METHOD__);
				} catch (Throwable $e) {
					Craft::warning("Could not clear the directory {$action}: " . $e->getMessage(), __METHOD__);
				}
			} else if (isset($cacheOption['params'])) {
				call_user_func_array($action, $cacheOption['params']);
			} else {
				$action();
			}
		}

		$cache = Craft::$app->getCache();
		TagDependency::invalidate($cache, 'template');

		return $this->asJson([
			'status' => 'success'
		]);
	}
}
